<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserModuloTest extends TestCase
{
    /**
     * Prueba http de respuesta en la ruta /usuarios
     * evalua si esta contiene 'usuarios'
     *
     * @test
     */
    function usuariosRoute()
    {
        $this->get("/usuarios")
            ->assertStatus(200)
            ->assertSee('usuarios');
    }

    /**
     * Prueba http de respuesta en la ruta /usuarios/5
     * evalua si esta contiene 'Accedio al usuario con id:5'
     *
     * @test
     */
    function usuariosDetalleRoute(){
        $this->get('/usuarios/5')
        ->assertStatus(200)
        ->assertSee('Accedio al usuario con id: 5');
    }
}
