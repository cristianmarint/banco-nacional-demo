<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class reciboModuloTest extends TestCase
{
    /**
     * Prueba http de respuesta y calculo de recibo para usuario y subTotal
     *
     * @test
     */
    public function reciboDetalleRoute(){
        $this->get('/recibo/presentador/30')
            ->assertStatus(200)
            ->assertSee('presentador su total a pagar es de 33');
    }
}
