<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Retorna usuarios
 */
Route::get('/usuarios', function () {
    return 'usuarios';
});


/**
 * Detalla el usuario accedido
 */
Route::get('/usuarios/{id}', function ($id) {
    return "Accedio al usuario con id: {$id}";
})->where("id","[0-9]+");


/**
 * Retorna el total a pagar para un recibo
 * se agrega un impuesto de 3 pesos al subtotal
 *
 * @Param string $username
 * @Param float  $subTotal
 */
route::get("/recibo/{username}/{subTotal}","reciboController@verRecibo")
    ->where(["username"=>"[A-Za-z]+","subTotal","[0-9]+"]);
